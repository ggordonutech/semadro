package jm.edu.utech.semantic;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test public void test_semantic_algorithms(){
        List<String> results = jm.edu.utech.semantic.TestDemi.runReturn("car","car");

        assertNotNull(results);
    }
}